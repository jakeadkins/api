document.getElementById("myBtn").addEventListener('click', fetchAPI);
let count = 1;

function fetchAPI(event) {
  event.preventDefault();
  let result = document.getElementById("stock").value;
  let key = '?apikey=0e93fdec9056ef6af20874c195ef16c6';
  let url = 'https://financialmodelingprep.com/api/v3/company/profile/' + result + key
  console.log(url);
  fetch(url)
    .then((response) => response.json())
    .then(function (data) {
      console.log(data);
      let profile = [];
      profile = data;
      let div = document.getElementById("firstList")
      let ul = document.createElement("ul");
      div.appendChild(ul);
      let li = document.createElement("li");
      let anchor = document.createElement('a');
      let a = document.createElement("a");
      a.innerText = "Company Website: " + data.profile.website;
      a.setAttribute('href', data.profile.website);
      ul.appendChild(a);
      li.innerHTML = "Company Name: " + data.profile.companyName;
      ul.appendChild(li);
      let li1 = document.createElement("li");
      li1.innerHTML = "Exchange: " + data.profile.exchange;
      ul.appendChild(li1);
      let li2 = document.createElement("li");
      li2.innerHTML = "Industry: " + data.profile.industry;
      ul.appendChild(li2);
      let li4 = document.createElement("li");
      li4.innerHTML = "Description: " + data.profile.description;
      ul.appendChild(li4);
      let li5 = document.createElement("li");
      li5.innerHTML = "CEO: " + data.profile.ceo;
      ul.appendChild(li5);
      let li6 = document.createElement("li");
      li6.innerHTML = "Sector: " + data.profile.sector;
      ul.appendChild(li6);
      let li7 = document.createElement("li");
      li7.innerHTML = "Price: N/A";
      if (data.profile.price > 0) {
        li7.innerHTML = "Price: $" + data.profile.price;

      }
      ul.appendChild(li7);
      let li8 = document.createElement("li");
      li8.innerHTML = "Beta: " + data.profile.beta;
      ul.appendChild(li8);
      let li9 = document.createElement("li");
      li9.innerHTML = "VolAvg: " + data.profile.volAvg;
      ul.appendChild(li9);
      let li10 = document.createElement("li");
      li10.innerHTML = "MKTCap: " + data.profile.mktCap;
      ul.appendChild(li10);
      let li11 = document.createElement("li");
      li11.innerHTML = "LastDiv: " + data.profile.lastDiv;
      ul.appendChild(li11);
      let li12 = document.createElement("li");
      li12.innerHTML = "Range: " + data.profile.range;
      ul.appendChild(li12);

      let li13 = document.createElement("li");
      li13.id = "liChanges";
      let changes = "Changes: " + data.profile.changes;
      li13.innerHTML = changes.fontcolor("green");
      if (data.profile.changes < 0) {
        let redchange = changes.fontcolor("red");
        li13.innerHTML = redchange;
      }
      ul.appendChild(li13);

      let li14 = document.createElement("li");
      li14.id = "liChangesPercent";
      let changesPercentage = "Changes Percentage: " + data.profile.changesPercentage;

      li14.innerHTML = changesPercentage.fontcolor("green");
      console.log(data.profile.changesPercentage);
      if (data.profile.changesPercentage.includes("-")) {
        let redChangesPercentage = changesPercentage.fontcolor("red");
        li14.innerHTML = redChangesPercentage;
      }

      ul.appendChild(li14);

    });
};